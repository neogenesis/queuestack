using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using Ionic.Zip;
using System.Diagnostics;

namespace QueueStackInstaller
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("QueueStackMod v1 by cozyGalvinism");
            string baseName = "The Genesis Project";
            if(!File.Exists("The Genesis Project.exe"))
            {
                if(args.Length == 1)
                {
                    if (!args[0].EndsWith(".exe"))
                    {
                        Console.WriteLine("Please put me in the same folder as \"The Genesis Project.exe\"!");
                        Console.ReadLine();
                        return;
                    }
                    baseName = Path.GetFileNameWithoutExtension(args[0]);
                    
                }
                else
                {
                    Console.WriteLine("Please put me in the same folder as \"The Genesis Project.exe\"!");
                    Console.ReadLine();
                    return;
                }
            }
            if (!Directory.Exists(baseName + "_Data"))
            {
                Console.WriteLine("The \"" + baseName + "_Data\" folder is missing! Aborting!");
                Console.ReadLine();
                return;
            }
            Console.WriteLine("Downloading most recent version...");
            WebClient wc = new WebClient();
            wc.DownloadFile("https://cozy.galvinism.ink/QueueStackMod.zip", "QueueStackMod.zip");
            Console.WriteLine("Extracting...");
            ZipFile zip = new ZipFile("QueueStackMod.zip");
            zip.ExtractAll(".", ExtractExistingFileAction.OverwriteSilently);
            ProcessStartInfo info = new ProcessStartInfo()
            {
                UseShellExecute = false,
                FileName = baseName + "_Data\\Managed\\MonoMod.exe",
                WorkingDirectory = baseName + "_Data\\Managed\\",
                Arguments = "Assembly-CSharp.dll",
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };
            Console.WriteLine("Patching (another window will open)...");
            Process p = Process.Start(info);
            while(!p.StandardOutput.EndOfStream)
            {
                Console.WriteLine(p.StandardOutput.ReadLine());
            }
            p.WaitForExit();
            Console.WriteLine("Done! Backing and cleaning up...");
            if (!Directory.Exists(baseName + "_Data\\Managed\\Backup")) Directory.CreateDirectory(baseName + "_Data\\Managed\\Backup");
            File.Copy(baseName + "_Data\\Managed\\Assembly-CSharp.dll", baseName + "_Data\\Managed\\Backup\\Assembly-CSharp.dll");
            File.Delete(baseName + "_Data\\Managed\\Assembly-CSharp.dll");
            File.Move(baseName + "_Data\\Managed\\MONOMODDED_Assembly-CSharp.dll", baseName + "_Data\\Managed\\Assembly-CSharp.dll");
            File.Delete("QueueStackMod.zip");
            Console.WriteLine("Enjoy! Press Enter to exit!");
            Console.ReadLine();
        }
    }
}
