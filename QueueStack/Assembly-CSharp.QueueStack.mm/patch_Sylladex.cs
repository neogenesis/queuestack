﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

class patch_Sylladex : Sylladex
{
    [MonoMod.MonoModConstructor()]
    public patch_Sylladex()
    {
        modi = new List<string>()
        {
            "Queue",
            "Stack",
            "Array",
            "Hashmap",
            "Tree",
            "QueueStack"
        };
        metrics = new char[]
        {
            'k',
            'M',
            'G',
            'T',
            'P'
        };
    }
    private extern void orig_Start();
    private new void Start()
    {
        this.strifeSpecibus.sylladex = this;
        int count = this.abilityButton.Count;
        Vector2 sizeDelta = new Vector2(128f, 64f);
        for (int i = 0; i < this.player.abilities.Count; i++)
        {
            if (i >= count)
            {
                this.abilityButton.Add(UnityEngine.Object.Instantiate<Button>(this.abilityButton[0]));
                this.abilityButton[i].transform.SetParent(this.abilityButton[0].transform.parent);
                this.abilityButton[i].GetComponent<RectTransform>().sizeDelta = sizeDelta;
                this.cooldownVisual.Add(this.abilityButton[i].transform.GetChild(1).GetComponent<Image>());
            }
            this.abilityButton[i].transform.localPosition = new Vector2(sizeDelta.x * (float)i + sizeDelta.x / 2f * (float)(1 - this.player.abilities.Count), this.abilityButton[0].transform.localPosition.y);
            this.abilityButton[i].GetComponentInChildren<Text>().text = '>' + this.player.abilities[i].name;
            this.abilityButton[i].GetComponentInChildren<Text>().color = this.player.abilities[i].color;
            float H;
            float S;
            float V;
            Color.RGBToHSV(this.player.abilities[i].color, out H, out S, out V);
            Material material = new Material(this.abilityButton[i].image.material);
            material.SetFloat("_HueShift", H * 360f);
            material.SetFloat("_Sat", S);
            material.SetFloat("_Val", V);
            this.abilityButton[i].image.material = material;
            this.abilityButton[i].name = "Ability " + this.player.abilities[i].name;
        }
        foreach (string str in Sylladex.GetModi())
        {
            string modus = str;
            if (modus == "QueueStack")
            {
                Image img = UnityEngine.Object.Instantiate<Image>(this.modusSwitchOption, this.modusSwitchOption.transform.parent);
                // img.sprite = UnityEngine.Resources.Load<Sprite>("Modi/ArrayModus");
                img.sprite = ModUtils.LoadSpriteFromFile("QueueStackMod/QueueStack.png");
                img.name = "QueueStackModus";
                img.GetComponent<Button>().onClick.AddListener((UnityAction)(() =>
                {
                    this.SetModus(modus, true);
                    img.transform.parent.parent.gameObject.SetActive(false);
                }));
            }
            else
            {
                Image img = !((UnityEngine.Object)this.modusSwitchOption.sprite == (UnityEngine.Object)null) ? UnityEngine.Object.Instantiate<Image>(this.modusSwitchOption, this.modusSwitchOption.transform.parent) : this.modusSwitchOption;
                img.sprite = UnityEngine.Resources.Load<Sprite>("Modi/" + modus + "Modus");
                img.name = modus + "Modus";
                img.GetComponent<Button>().onClick.AddListener((UnityAction)(() =>
                {
                    this.SetModus(modus, true);
                    img.transform.parent.parent.gameObject.SetActive(false);
                }));
            }
        }
        if (!((UnityEngine.Object)this.captchaModus == (UnityEngine.Object)null))
            return;
        this.SetModus(ChangeSpritePart.modus, false);
    }

    public extern void orig_SetModus(string modus, bool playSound = false);
    public new void SetModus(string modus, bool playSound = false)
    {
        if (modus != null)
        {
            if (modus == "Stack")
            {
                this.captchaModus = this.modusObject.AddComponent<FILOModus>();
            }
            if (modus == "Array")
            {
                this.captchaModus = this.modusObject.AddComponent<ArrayModus>();
            }
            if (modus == "Hashmap")
            {
                this.captchaModus = this.modusObject.AddComponent<HashmapModus>();
            }
            if (modus == "Tree")
            {
                this.captchaModus = this.modusObject.AddComponent<TreeModus>();
            }
            if (modus == "Queue")
            {
                this.captchaModus = this.modusObject.AddComponent<FIFOModus>();
            }
            if (modus == "QueueStack")
            {
                this.captchaModus = this.modusObject.AddComponent<QueueStackModus>();
            }
            this.modusName = modus;
            if (playSound)
            {
                this.PlaySoundEffect(this.clipSwitch);
            }
        }
    }

    public extern void orig_AddModus(string modus);
    public new void AddModus(string modus)
    {
        if (modus == "QueueStack")
        {
            if (Sylladex.GetModi().Contains("QueueStack")) return;
            Sylladex.GetModi().Add(modus);
            Image img = UnityEngine.Object.Instantiate<Image>(this.modusSwitchOption, this.modusSwitchOption.transform.parent);
            // img.sprite = UnityEngine.Resources.Load<Sprite>("Modi/ArrayModus");
            img.sprite = ModUtils.LoadSpriteFromFile("QueueStackMod/QueueStack.png");
            img.name = "QueueStackModus";
            img.GetComponent<Button>().onClick.AddListener((UnityAction)(() =>
            {
                this.SetModus(modus, true);
                img.transform.parent.parent.gameObject.SetActive(false);
            }));
        }
        else
        {
            orig_AddModus(modus);
        }
    }

    private Modus captchaModus;
    private GameObject modusObject;
    private string modusName;
    private AudioClip clipSwitch;
    private List<string> modi;
    private char[] metrics;
}
