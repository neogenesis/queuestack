﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

class QueueStackModus : Modus
{
    private LinkedList<Modus.Card> itemList = new LinkedList<Modus.Card>();

    private new void Awake()
    {
        base.Awake();
        this.itemCapacity = 8;
        this.separation = new Vector2((float)(-(double)this.complexcardsize.x / 4.0), this.complexcardsize.y / 4f);
        this.SetColor(new Color((float)byte.MaxValue, 128f, 0.0f));
        SetIconFromPath("QueueStackMod/QueueStack.png");
    }

    protected void SetIconFromPath(string path)
    {
        this.sylladex.modusIcon.sprite = ModUtils.LoadSpriteFromFile(path);
    }

    protected override bool AddItemToModus(Item toAdd)
    {
        if (this.itemList.Count == this.itemCapacity)
        {
            return false;
        }
        else
        {
            this.itemList.AddFirst(MakeCard(toAdd, this.itemList.Count, -1));
        }
        return true;
    }

    protected override bool IsRetrievable(Modus.Card item)
    {
        if (this.itemList.Count != 0)
            return item == this.itemList.First() || item == this.itemList.Last();
        return false;
    }

    protected override IEnumerable<Modus.Card> GetItemList()
    {
        return this.itemList;
    }

    public override void Save(Stream stream)
    {
        base.Save(stream);
    }

    public override void Load(Stream stream)
    {
        base.Load(stream);
    }

    protected override void Load(Item[] items)
    {
        this.itemList = new LinkedList<Modus.Card>();
        for (int i = 0; i < items.Length; i++)
        {
            this.itemList.AddFirst(MakeCard(items[i], i, 1));
        }
    }

    public override int GetAmount()
    {
        return this.itemList.Count;
    }

    protected override bool RemoveItemFromModus(Modus.Card item)
    {
        if (IsRetrievable(item))
        {
            this.itemList.Remove(item);
            return true;
        }
        return false;
    }
}
