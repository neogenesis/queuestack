﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

class ModUtils
{
    public static Sprite LoadSpriteFromFile(string path)
    {
        Sprite spr = new Sprite();
        Texture2D tex = LoadTexture(path);
        spr = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0));
        return spr;
    }

    public static Texture2D LoadTexture(string path)
    {
        Texture2D tex2d;
        byte[] file;

        if(File.Exists(path))
        {
            file = File.ReadAllBytes(path);
            tex2d = new Texture2D(2, 2);
            if(tex2d.LoadImage(file))
            {
                return tex2d;
            }
        }
        return null;
    }
}