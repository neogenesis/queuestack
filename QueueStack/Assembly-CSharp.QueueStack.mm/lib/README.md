# Libs

Please place the following files from your `The Genesis Project_Data\Managed` in here:

* Assembly-CSharp.dll
* UnityEngine.AudioModule.dll
* UnityEngine.CoreModule.dll
* UnityEngine.dll
* UnityEngine.ImageConversionModule.dll
* UnityEngine.UI.dll