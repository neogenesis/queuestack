# QueueStack

Implements the QueueStack from Homestuck into TGP!

## How to install

### Automatic installation

1. Download the latest version of the installer from the most recent tag
2. Extract the contents of the zip file into your game folder (the forlder where `The Genesis Project.exe` or similar resides in)
3. Run the installer
4. ???
5. Enjoy!

### Manual installation

0. Back up your TGP installation!!! If you don't do this, you will have to redownload the game!
1. Download the latest version of the mod from [here](https://cozy.galvinism.ink/QueueStackMod.zip)
2. Extract the zip file into the root of the game (where `The Genesis Project.exe` resides in)
3. Go into `The Genesis Project_Data\Managed`
4. Drag `Assembly-CSharp.dll` onto MonoMod.exe
5. This should create a file called `MONOMODDED_Assembly-CSharp.dll`
6. Delete `Assembly-CSharp.dll` (or move it out of the folder) and rename `MONOMODDED_Assembly-CSharp.dll` to `Assembly-CSharp.dll`
7. Start the game and enjoy

**Important:** The modus does NOT show up in the character creation screen! Select a different mode and then switch to QueueStack once you're in the game!

## How to uninstall

0. Check if you still have access to your backed up installation (if you used the installer, there is a backup folder in `The Genesis Project_Data\Managed`, which contains your unmodified `Assembly-CSharp.dll`)
1. In your modded installation, delete the `The Genesis Project_Data\Managed\Assembly-CSharp.dll` file
2. From your backup, copy that same file into your modded installation
3. Your previously modded installation is now unmodded

## How to build

1. Clone this repository recursively (to clone MonoMod too)
2. Place the necessary files (as listed in [here](QueueStack/Assembly-CSharp.QueueStack.mm/lib/)) and build the solution. Not just the project, as MonoMod is required to be in the loaded solution.
3. Build

This creates a file (along with MonoMod) called `Assembly-CSharp.QueueStack.mm.dll`. Copy that file and `MonoMod.exe`, `Mono.Cecil.dll`, `Mono.Cecil.Mdb.dll`, `Mono.Cecil.Pdb.dll` and `MonoMod.Utils.dll` into your `The Genesis Project_Data\Managed` folder and proceed with step 4 of **How to install**

## Credits

- QueueStack.png - by Marrow